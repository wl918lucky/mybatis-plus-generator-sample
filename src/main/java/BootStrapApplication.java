import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class BootStrapApplication {
    public static void main(String[] args) {
        String projectPath = System.getProperty("user.dir");

        FastAutoGenerator.create("jdbc:mysql://localhost:3306/keepup?useSSL=false&serverTimezone=UTC&tinyInt1isBit=false", "root", "root")
                .globalConfig(builder -> {
                    builder.author("Jerry")
                            .enableSwagger()
                            .disableOpenDir()
                            .fileOverride()
                            .dateType(DateType.ONLY_DATE)
                            .outputDir(projectPath + "/src/main/java");
                })
                .packageConfig(builder -> {
                            // 设置父包名
                    builder.parent("com.wl918lucky")
                            // 设置父包模块名
                            .moduleName("dal")
                            // 设置mapperXml生成路径
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, projectPath + "/src/main/resources/mapper"));
                })
                .strategyConfig(builder -> {
                            // 设置需要生成的表名,所需要转换的数据表
                    builder.addInclude("tb_user")
                            // 设置过滤表前缀,配置后生成实体将去掉前缀
                            .addTablePrefix("t_","tb_","c_");
                    builder.entityBuilder()
                            .enableLombok()
                            .enableChainModel()
                            .enableTableFieldAnnotation()
                            .addTableFills(new Column("gmt_create", FieldFill.INSERT))
                            .addTableFills(new Column("gmt_modify", FieldFill.INSERT_UPDATE))
                            .idType(IdType.AUTO)
                            .formatFileName("%sDO")
                            .build();
                    builder.serviceBuilder()
                            .formatServiceFileName("%sService")
                            .formatServiceImplFileName("%sServiceImpl")
                            .build();
                    builder.mapperBuilder()
                            .enableBaseResultMap()
//                            .enableMapperAnnotation()
                            .build();
                })
                .templateConfig(builder -> {
                    builder.entity("/templates/entity.java")
                            .build();
                })
                .injectionConfig(builder -> {
                    Map<String, Object> map = new HashMap<>();
                    Map<String, Object> cfg = new HashMap<>();
                    cfg.put("abc", 123);
                    map.put("cfg",cfg);
                    builder.beforeOutputFile((tableInfo, objectMap) -> {
                                System.out.println("tableInfo: " + tableInfo.getEntityName() + " objectMap: " + objectMap.size());
                            })
                            .customMap(map)
                              //生成指定文件样例
//                            .customFile(Collections.singletonMap("test.txt", "/templates/test.vm"))
                            .build();
                })
                // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();

    }
}
